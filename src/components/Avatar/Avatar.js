import React, {Component} from 'react'
import './avatar.scss'

export default class Avatar extends Component{
    constructor(props){
        super(props)
    }
    render() {
        const {src, width, height} = this.props
        return src && <img width={width} height={height} src ={src} className='avatar'/>
    }
}