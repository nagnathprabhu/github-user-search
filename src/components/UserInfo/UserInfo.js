import React, { Component } from 'react'
import Avatar from '../Avatar/Avatar'
import Loader from '../Loader/Loader'
import './userInfo.scss'

export default class UserInfo extends Component {
    constructor(props) {
        super(props)

    }

    renderFollowers = (followers = []) => {
        return followers.length && <div className='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
            <h3 className='section-title'>Followers</h3>
            {followers.map(ele => <div className='follower-item'>
                <Avatar width='40px' height='60px' src={ele.avatar_url} />
                <span className='follower-title'>{ele.login}</span>
            </div>
            )}
        </div> || ''
    }

    renderRepos = (repos = []) => {
        return repos.length && <div className='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
            <h3 className='section-title'>Repos</h3>
            {repos.map(ele => <div className='repo-item'>
                <h4 className='repo-title'>{ele.name} {ele.language && `(${ele.language})` || ''} </h4>
                <span className='repo-description'>{ele.description || ''}</span>
            </div>
            )}
        </div> || ''
    }
    render() {
        const { user, pending } = this.props
        return <div className='row' className='user-info-section'>
            {
                pending && <Loader /> ||
                <>
                    <div >
                        {user['avatar_url'] && <Avatar src={user['avatar_url']} width='150px' height='150px' /> || ''}
                        {user['login'] && <span className='user-name'>{user.login}</span>}
                    </div>
                    {
                        this.renderFollowers(user['followers'])
                    }
                    {
                        this.renderRepos(user['repos'])
                    }
                </>
            }
        </div>
    }
}