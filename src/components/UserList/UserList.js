import React, { Component } from 'react'
import './userList.scss'
import Avatar from '../Avatar/Avatar'
import Loader from '../Loader/Loader'
export default class UserList extends Component {
    constructor(props) {
        super(props);
    }

    viewUser = (e, userName) => {
        this.props.viewUser(e, userName)
    }

    onChange = e => {
        this.props.filter(e.target.value)
    }

    render() {
        const { users, pending } = this.props
        return <>
            <div className="form-group ">
                <input type="text" className="search form-control" placeholder="Who are you looking for?" onChange={this.onChange} />
            </div>

            {
                pending && <Loader /> || <div className='user-info'>
                    <table className='table table-striped '>
                        <thead>
                            <tr>
                                <th>Avatar</th>
                                <th>Username</th>
                                <th>Github link</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                users.map((ele, index) => <tr key={index}>
                                    <td><Avatar src={ele.avatar_url} width='50px' /></td>
                                    <td><a onClick={(e) => { this.viewUser(e, ele.login) }}>{ele.login}</a></td>
                                    <td><a href={ele.html_url}>click here</a></td>
                                </tr>)
                            }
                        </tbody>
                    </table>
                </div>
            }
        </>
    }
}