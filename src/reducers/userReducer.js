// import {FETCH_IMAGES_PENDING, FETCH_IMAGES_SUCCESS, FETCH_IMAGES_ERROR} from './actions/imageAction';
import ActionTypes from '../actions/ActionTypes'
const initialState = {
    userPending: false,
    users: [],
    error: null,
    listPending: false,
    filteredUsers: [],
    user: {}
}

export default function userReducer(state = initialState, action) {
    switch (action.type) {
        case ActionTypes.USERS.FETCH_USER_LIST_PENDING.name:
            return {
                ...state,
                listPending: true
            }
        case ActionTypes.USERS.FETCH_USER_LIST_SUCCESS.name:
            return {
                ...state,
                listPending: false,
                users: action.payload
            }
        case ActionTypes.USERS.FETCH_USER_LIST_ERROR.name:
            return {
                ...state,
                listPending: false,
                error: action.error
            }
        case ActionTypes.USERS.FETCH_INDIVIDUAL_USER_PENDING.name:
            return {
                ...state,
                userPending: true
            }
        case ActionTypes.USERS.FETCH_INDIVIDUAL_USER_SUCCESS.name:
            return {
                ...state,
                userPending: false,
                user: action.payload
            }
        case ActionTypes.USERS.FETCH_INDIVIDUAL_USER_ERROR.name:
            return {
                ...state,
                userPending: false,
                error: action.error
            }
        case ActionTypes.USERS.FILTER_USER.name:
            return {
                ...state,
                filteredUsers: state.users.filter(ele => ele.login.includes(action.user))
            }
        default:
            return state;
    }
}
