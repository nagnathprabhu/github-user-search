import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';
import UserList from './components/UserList/UserList'
import UserInfo from './components/UserInfo/UserInfo'
import { fetchUsers, viewUser, filter } from './actions/userAction'



class App extends Component {
    componentDidMount() {
        this.props.fetchUsers()
    }

    viewUser = (e, userName) => {
        e.preventDefault()
        let user = this.props.viewUser(userName)
    }

    filter = user => {
        this.props.filter(user)
    }

    render() {
        const { users, user, listPending, userPending, filteredUsers } = this.props;
        return (
            <div className="App">
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-xs-12 col-sm-12 col-md-4 col-lg-4'>
                            <UserList users={filteredUsers.length && filteredUsers || users} viewUser={this.viewUser} pending={listPending} filter={this.filter} />
                        </div>
                        <div className='col-xs-12 col-sm-12 col-md-8 col-lg-8'>
                            <UserInfo user={user} pending={userPending} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchUsers: () => dispatch(fetchUsers()),
        viewUser: (user) => dispatch(viewUser(user)),
        filter: (user) => dispatch(filter(user))
    }
}

const mapStateToProps = (state) => {
    const { userReducer: { users, error, user, listPending, userPending, filteredUsers } } = state
    return {
        users,
        error,
        user,
        listPending,
        userPending,
        filteredUsers
    }

}




export default connect(mapStateToProps, mapDispatchToProps)(App);