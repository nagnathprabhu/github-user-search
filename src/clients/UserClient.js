import axios from 'axios'

export default class UserClient {
    constructor() {
    }

    // fetching only the initial list of users. The requirement on how to handle pagination wasn't mentioned in the requirement doc
    async fetchUsers() {
        let users = await axios.get('https://api.github.com/users')
        return users
    }

    async fetchUser(userName) {
        try {
            let user = (await axios.get(`https://api.github.com/users/${userName}`))['data']
            let repos = user['repos_url'] && await this.fetchUserMeta(user['repos_url']) || []
            let followers = user['followers_url'] && await this.fetchUserMeta(user['followers_url']) || []
            user['repos'] = repos
            user['followers'] = followers
            return { ...user, repos, followers }
        } catch (e) {
            throw e
        }

    }

    async fetchUserMeta(url) {
        let userMeta = (await axios.get(url))['data']
        return userMeta
    }

}