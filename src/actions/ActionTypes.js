const ACTIONS = {
    USERS: {
        FETCH_USER_LIST_PENDING:{
            name:"FETCH_USER_LIST_PENDING"
        },
        FETCH_USER_LIST_SUCCESS:{
            name:"FETCH_USER_LIST_SUCCESS"
        },
        FETCH_USER_LIST_ERROR:{
            name:"FETCH_USER_LIST_ERROR"
        },
        FETCH_INDIVIDUAL_USER_SUCCESS:{
            name:"FETCH_INDIVIDUAL_USER_SUCCESS"
        },
        FETCH_INDIVIDUAL_USER_PENDING:{
            name:"FETCH_INDIVIDUAL_USER_PENDING"
        },
        FETCH_INDIVIDUAL_USER_ERROR:{
            name:"FETCH_INDIVIDUAL_USER_ERROR"
        },
        FILTER_USER: {
            name:"FILTER_USER"
        }
    }
}
export default ACTIONS