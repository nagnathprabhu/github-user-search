import UserClient from '../clients/UserClient'

const fetchUserListStart = () => {
    return {
        type: "FETCH_USER_LIST_PENDING"
    }
}

const fetchUserListSuccess = (payload) => {
    return {
        type: "FETCH_USER_LIST_SUCCESS",
        payload
    }
}
const fetchIndividualUserSuccess = (payload) => {
    return {
        type: "FETCH_INDIVIDUAL_USER_SUCCESS",
        payload
    }
}

const fetchIndividualUserStart = (payload) => {
    return {
        type: "FETCH_INDIVIDUAL_USER_PENDING",
        payload
    }
}

const fetchIndividualUserError = (error) => {
    return {
        type: "FETCH_INDIVIDUAL_USER_ERROR",
        error
    }
}

const fetchUserListError = (error) => {
    return {
        type: "FETCH_USER_LIST_ERROR",
        error
    }
}

const filterUser = (user) => {
    return {
        type: "FILTER_USER",
        user
    }
}


export const fetchUsers = () => dispatch => {
    dispatch(fetchUserListStart())
    let ic = new UserClient()
    return ic.fetchUsers().then((data) => dispatch(fetchUserListSuccess(data['data']))).catch(e => dispatch(fetchUserListError(e)))
}

export const viewUser = (userName) => dispatch => {
    dispatch(fetchIndividualUserStart())
    let ic = new UserClient()
    return ic.fetchUser(userName).then((data) => dispatch(fetchIndividualUserSuccess(data))).catch(e => dispatch(fetchIndividualUserError(e)))
}

export const filter = (user) => dispatch => {
    dispatch(filterUser(user))
}
